# Reactive Web Component

This is just a test to compare work needed and amount of control over each state when creating reactive components using Svelte versus native Web Components, basically I made the same "Counter" component twice, which you can compare at `src/lib/CounterSvelte.svelte` and `src/lib/CounterWebComponent.svelte`.

## Running The Project

To run and test this project you need:

- Node 18+

Simply run `npm install` in the root of the project and then run `npm run dev` to spin up the server which you can check at `http://localhost:5173/`.

## Hacky Web Component

I've also added a "Hacky Way" of doing Web Components, where the code is a little simpler but it re-renders the entire component for every attribute change, so it's a bit heavier but much easier to read and understand, please refer to `src/lib/CounterWebComponentHacky.svelte`.